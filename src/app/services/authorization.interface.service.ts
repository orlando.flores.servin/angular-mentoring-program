import { User } from '../models/user';
import { Observable } from 'rxjs';

export interface  AuthorizationService {
  login(user: User): Observable<any>;
  logout(): boolean;
  isAuthenticated(): boolean;
  getUserInfo(fakeToken: string): Observable<any>;
}
