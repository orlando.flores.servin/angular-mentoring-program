import { Injectable, EventEmitter } from '@angular/core';
import { Course } from '../models/course';
import { CourseService } from './course.interface.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CourseServiceImpl implements CourseService {
  courses: Course [];
  angularCourse: Course;
  springBootCourse: Course;
  tddCourse: Course;
  start = 0;
  count = 2;

  onCreateCourse = new EventEmitter();

  constructor(private httpClient: HttpClient) {
    this.courses = [];
  }

  getCourses() {
    let urlParams = new HttpParams();
    urlParams = urlParams.append('start', `${this.start}` );
    urlParams = urlParams.append('count', `${this.count}`);
    urlParams = urlParams.append('sort', `date`);
    return this.httpClient.get(`${environment.LOCAL_SERVER}/courses`, {params: urlParams});
  }

  searchCourseByTextFragment(textFragment: string) {
    let urlParams = new HttpParams();
    urlParams = urlParams.append('start', `${this.start}` );
    urlParams = urlParams.append('count', `${this.count}`);
    urlParams = urlParams.append('textFragment', `${textFragment}`);
    return this.httpClient.get(`${environment.LOCAL_SERVER}/courses`, {params: urlParams});
  }

  createCourse(course: Course) {
    course.id = Math.floor((Math.random() * 1000000) + 1100000);
    return this.httpClient.post(`${environment.LOCAL_SERVER}/courses`, course);
  }

  getItemById(id: number) {
    return this.httpClient.get(`${environment.LOCAL_SERVER}/courses/${id}`);
  }

  updateItem(course: Course) {
    return this.httpClient.patch(`${environment.LOCAL_SERVER}/courses/${course.id}`, course);
  }

  removeItem( id: number) {
    return this.httpClient.delete(`${environment.LOCAL_SERVER}/courses/${id}`);
  }
}
