import { TestBed } from '@angular/core/testing';

import { AuthorizationServiceImpl } from './authorization.service';

describe('AuthorizationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [AuthorizationServiceImpl]
  }));

  it('should be created', () => {
    const service: AuthorizationServiceImpl = TestBed.get(AuthorizationServiceImpl);
    expect(service).toBeTruthy();
  });
});
