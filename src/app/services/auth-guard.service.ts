import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthorizationServiceImpl } from './authorization.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  constructor(private authorizationServiceImpl: AuthorizationServiceImpl, private router: Router) {
  }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let hasAccess: boolean;
    hasAccess = this.authorizationServiceImpl.isAuthenticated();
    if ( !hasAccess ) {
      this.router.navigate(['login']);
    }
    return hasAccess;
  }
}
