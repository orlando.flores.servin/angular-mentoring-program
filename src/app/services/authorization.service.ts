import { Injectable } from '@angular/core';
import { AuthorizationService } from './authorization.interface.service';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import { Observable } from 'rxjs';
import * as CourseActions from './../app.actions';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationServiceImpl implements AuthorizationService {

  userRedux: Observable<any>;

  constructor(private httpClient: HttpClient, private store: Store<AppState>) { 
    this.userRedux = store.select('courses');
  }

  login(user: User) {
    return this.httpClient.post(`${environment.LOCAL_SERVER}/auth/login`, {
      login: user.login,
      password: user.password
    });
  }

  logout(): boolean {
    localStorage.clear();
    return true;
  }

  isAuthenticated(): boolean {
    let authenticated = false;
    this.userRedux.subscribe(u => {
      authenticated = u.user != null;
      console.log(`isAuthenticated: ${authenticated} , ${u.user}`);
    });
    return authenticated;
  }

  getUserInfo(fakeToken: string) {
    return this.httpClient.post(`${environment.LOCAL_SERVER}/auth/userinfo`, {
      token: fakeToken
    });
  }

  getUserInfoFromLocalStorage(): User {
    return JSON.parse( localStorage.getItem( 'user'));
  }

  setUserInfoOnLocalStorage(user: User): void {
    localStorage.setItem( 'user', JSON.stringify(user) );
  }
}
