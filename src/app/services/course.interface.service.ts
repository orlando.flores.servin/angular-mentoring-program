import { Course } from '../models/course';
import { Observable } from 'rxjs';

export interface CourseService {
  getCourses();
  searchCourseByTextFragment(textFragment: string);
  createCourse(course: Course);
  getItemById(id: number);
  updateItem(course: Course);
  removeItem(id: number);
}
