import { TestBed } from '@angular/core/testing';
import { CourseServiceImpl } from './course.service';
import { Course } from '../models/course';

describe('CourseService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [CourseServiceImpl] });
  });

  it('should be created', () => {
    const service: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    expect(service).toBeTruthy();
  });

  it('should return a List of Courses with more than one element', () => {
    const service: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    expect(service.getCourses().length).toBeGreaterThan(0);
  });

  it('should add a new course', () => {
    const courseService: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    const devopsCourse: Course =  {
      id: 4,
      title: 'DEV-OPS',
      creationDate: new Date(2019, 10, 28, 0, 0, 0, 0),
      duration: 360,
      // tslint:disable-next-line: max-line-length
      description: 'DevOps is a set of practices that combines software development and information-technology operations which aims to shorten the systems development life cycle and provide continuous delivery with high software quality.',
      topRated: true
    };
    expect(courseService.createCourse(devopsCourse)).toBeTruthy();
  });

  it('should get a course by id', () => {
    const courseService: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    const id = 3;
    const course: Course = courseService.getItemById(id);
    expect(course.title).toEqual('TDD');
  });

  it('should not get a course by id', () => {
    const courseService: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    const id = 4;
    const course: Course = courseService.getItemById(id);
    expect(course).toBeUndefined();
  });

  it('should update a course', () => {
    const courseService: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    let courseUpdate: Course = {
      id: 2,
      title: 'Spring Boot - Spring Cloud',
      creationDate: new Date(2019, 6, 28, 0, 0, 0, 0),
      duration: 120,
      description: 'Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run"',
      topRated: false
    };
    expect(courseService.updateItem(courseUpdate)).toBeTruthy();
    courseUpdate = courseService.getItemById(courseUpdate.id);
    expect(courseUpdate.title).toEqual('Spring Boot - Spring Cloud');
  });

  it('should not update a course', () => {
    const courseService: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    const courseUpdate: Course = {
      id: 10,
      title: 'Spring Boot - Spring Cloud',
      creationDate: new Date(2019, 6, 28, 0, 0, 0, 0),
      duration: 120,
      description: 'Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run"',
      topRated: false
    };
    expect(courseService.updateItem(courseUpdate)).toBeFalsy();
  });

  it('should be removed a course', () => {
    const courseService: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    let course: Course = {
      id: 2,
      title: 'Spring Boot',
      creationDate: new Date(2019, 6, 28, 0, 0, 0, 0),
      duration: 120,
      description: 'Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run"',
      topRated: false
    };
    expect(courseService.removeItem(course.id)).toBeTruthy();
    course = courseService.getItemById(course.id);
    expect(course).toBeUndefined();
  });
});
