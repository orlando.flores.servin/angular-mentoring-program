import { SearchCoursePipe } from './search-course.pipe';
import { TestBed } from '@angular/core/testing';
import { Course } from '../models/course';
import { CourseServiceImpl } from '../services/course.service';

describe('SearchCoursePipe', () => {
  let pipe: SearchCoursePipe;
  let courses: Course [];

  beforeEach(() => {
    pipe = new SearchCoursePipe();
    TestBed.configureTestingModule({ providers: [CourseServiceImpl] });
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('shoud convert a minutes to duration format  for instance 60 minutes to 1h 0 min', () => {
    const service: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    courses = service.getCourses();
    const expectedResult: Course[] = [{
      id: 3,
      title: 'TDD',
      creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
      duration: 120,
      // tslint:disable-next-line: max-line-length
      description: 'Test-driven development (TDD) is a software development process that relies on the repetition of a very short development cycle, Test-driven development (TDD) is a software development process that relies on the repetition of a very short development cycle',
      topRated: true
    }];
    expect(pipe.transform(courses, 'TDD')[0].title).toBe(expectedResult[0].title);
  });
});
