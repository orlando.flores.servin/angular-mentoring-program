import { OderByTitlePipe } from './oder-by-title.pipe';
import { Course } from '../models/course';
import { TestBed } from '@angular/core/testing';
import { CourseServiceImpl } from '../services/course.service';

describe('OderByTitlePipe', () => {

  let pipe: OderByTitlePipe;
  let courses: Course [];

  beforeEach(() => {
    pipe = new OderByTitlePipe();
    TestBed.configureTestingModule({ providers: [CourseServiceImpl] });
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('shoud order the courses by title', () => {
    const service: CourseServiceImpl = TestBed.get(CourseServiceImpl);
    courses = service.getCourses();
    const expectedResult: Course[] = [{
      id: 3,
      title: 'TDD',
      creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
      duration: 120,
      description: 'Test-driven development (TDD) is a software development process',
      topRated: true
    }];
    expect(pipe.transform(courses, null)[2].title).toBe(expectedResult[0].title);
  });
});
