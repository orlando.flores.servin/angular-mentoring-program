import { Pipe, PipeTransform } from '@angular/core';
import { Course } from '../models/course';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'searchCourse'
})
export class SearchCoursePipe implements PipeTransform {
  datePipe: DatePipe;
  constructor() {

  }

  transform(courses: Course[], searchValue: string): Course[] {
    this.datePipe = new DatePipe('en-US');
    if (!courses) {return null; }
    if (!searchValue) {return courses; }
    searchValue = searchValue.toLowerCase();
    return courses.filter((course: Course) => {
      if (course.name.toLowerCase().includes(searchValue) ||
        // this.datePipe.transform(course.creationDate, 'dd MMM, yyyy').includes(searchValue) ||
          course.date.includes(searchValue) ||
          // course.creationDate.toLowerCase().includes(searchValue) ||
          this.getDuration(course.length).includes(searchValue) ||
          course.description.toLowerCase().includes(searchValue) ) {
        return course;
      }
    });
  }

  private getDuration(minutes: number): string {
    const h = minutes / 60;
    const min = minutes % 60;
    return h > 0 ?  (h + 'h' + ' ' + min + ' min') : ( min + ' min');
  }
}
