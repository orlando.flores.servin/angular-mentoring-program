import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})

export class DurationPipe implements PipeTransform {

  transform(minutes: number, ...args: any[]): any {
    let result = '0 min';
    if (minutes !== null && minutes > 0 ) {
      const h = Math.floor(minutes / 60);
      const min = minutes % 60;
      result = h > 0 ?  (h + 'h' + ' ' + min + ' min') : ( min + ' min');
    }
    return result;
  }
}

