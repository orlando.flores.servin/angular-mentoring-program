import { Pipe, PipeTransform } from '@angular/core';
import { Course } from '../models/course';

@Pipe({
  name: 'oderByTitle'
})
export class OderByTitlePipe implements PipeTransform {

  transform(courses: Course[], searchValue: string): Course[] {
    return courses.sort((a: Course, b: Course) => {
      if (a.name < b.name) {
        return -1;
      } else if (a.name > b.name) {
        return 1;
      } else {
        return 0;
      }
    });
  }
}
