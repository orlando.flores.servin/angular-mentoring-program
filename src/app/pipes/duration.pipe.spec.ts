import { DurationPipe } from './duration.pipe';

describe('DurationPipe', () => {
  let pipe: DurationPipe;

  describe('transform', () => {

    beforeEach(() => {
      pipe = new DurationPipe();
    });

    it('should create a DurationPipe instance', () => {
      expect(pipe).toBeTruthy();
    });

    it('shoud transform "60" to "1h 0 min"', () => {
      const minutes = 60;
      expect(pipe.transform(minutes)).toBe('1h 0 min');
    });

    it('shoud transform "30" to "30 min"', () => {
      const minutes = 30;
      expect(pipe.transform(minutes)).toBe('30 min');
    });

    it('shoud transform negative number "-10" to "0 min"', () => {
      const minutes = -10;
      expect(pipe.transform(minutes)).toBe('0 min');
    });
  });
});
