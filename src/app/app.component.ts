import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   // AOT Ahead-Of-Time Compilation is enable for in production, no error were found for
   // ng build --prod
   constructor(private translate: TranslateService) {
     translate.setDefaultLang('mx');
   }
}
