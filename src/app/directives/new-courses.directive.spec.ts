import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NewCoursesDirective } from './new-courses.directive';
import { FormsModule } from '@angular/forms';
import { CoursesComponent } from '../components/courses/courses.component';
import { CourseItemComponent } from '../components/courses/course-item/course-item.component';
import { LoadMoreComponent } from '../components/courses/load-more/load-more.component';
import { SectionComponent } from '../components/section/section.component';
import { NoDataMessageComponent } from '../components/no-data-message/no-data-message.component';
import { SearchCoursePipe } from '../pipes/search-course.pipe';
import { DurationPipe } from '../pipes/duration.pipe';
import { OderByTitlePipe } from '../pipes/oder-by-title.pipe';
import { CourseServiceImpl } from '../services/course.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Course } from '../models/course';
import { By } from '@angular/platform-browser';

describe('NewCoursesDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [
        CoursesComponent,
        CourseItemComponent,
        LoadMoreComponent,
        SectionComponent,
        NoDataMessageComponent,

        SearchCoursePipe,
        DurationPipe,
        OderByTitlePipe,
        NewCoursesDirective
      ],
      providers: [CourseServiceImpl, SearchCoursePipe],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));




  describe('NewCoursesDirective', () => {
    let coursesComponentFixture: ComponentFixture<CoursesComponent>;
    let coursesComponent: CoursesComponent;
    let angularCourse: Course;
    let tddCourse: Course;

    beforeEach(() => {
      coursesComponentFixture = TestBed.createComponent(CoursesComponent);
      coursesComponent = coursesComponentFixture.componentInstance;
      tddCourse = {
        id: 3,
        title: 'TDD',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD)',
        topRated: true
      };
      angularCourse = {
        id: 1,
        title: 'Angular 01',
        creationDate: new Date(2019, 10, 28, 0, 0, 0, 0),
        duration: 90,
        description: 'Angular is a platform for building mobile and desktop web applications.',
        topRated: false
      };
      coursesComponent.courses = [tddCourse, angularCourse];
      coursesComponentFixture.detectChanges();
    });

    it('should add a class .new-course depending on creationDate', () => {
      const newCourses = coursesComponentFixture.debugElement.query(By.css('.new-course'));
      expect(newCourses).toBeTruthy();
    });
  });
});
