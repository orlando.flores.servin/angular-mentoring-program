import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appNewCourses]'
})
export class NewCoursesDirective  implements OnInit {


  @Input() creationDate: Date;
  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

 ngOnInit() {
    const currentDateEpoch: number = new Date().getTime();
    const creationDateEpoch: number = this.creationDate.getTime();
    if (creationDateEpoch <= currentDateEpoch && creationDateEpoch >=  currentDateEpoch - (14 * 24 * 60 * 60 * 1000)) {
       this.renderer.addClass(this.elementRef.nativeElement, 'new-course');
    }
 }
}
