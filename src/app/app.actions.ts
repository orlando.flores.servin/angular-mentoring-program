import { Action } from '@ngrx/store';
import { Course } from './models/course';
import { User } from './models/user';

export const LOGIN       = 'LOGIN';
export const GET_USER       = 'GET_USER';

export const SET_COURSES       = 'SET_COURSES';
export const ADD_COURSES       = 'ADD_COURSES';


export class Login implements Action {
    readonly type = LOGIN;
    constructor(public payload: User) {}
}

export class GetUser implements Action {
    readonly type = GET_USER;
    constructor(public payload: User) {}
}

export class SetCourse implements Action {
    readonly type = SET_COURSES;
    constructor(public payload: Course[]) {}
}

export class AddCourse implements Action {
    readonly type = ADD_COURSES;
    constructor(public payload: Course) {}
}

export type Actions = Login | GetUser | SetCourse | AddCourse;
