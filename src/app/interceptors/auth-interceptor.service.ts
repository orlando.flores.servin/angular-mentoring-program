import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthorizationServiceImpl } from '../services/authorization.service';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private authorizationServiceImpl: AuthorizationServiceImpl) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user: User = this.authorizationServiceImpl.getUserInfoFromLocalStorage();

    let request: HttpRequest<any> = req;

    if (user !== null) {
      request = req.clone({
        setHeaders: {
          Authorization: `${user.fakeToken}`
          // Authorization: `Bearer ${user.fakeToken}`
        }
      });
    }

    return next.handle(request);
  }


}
