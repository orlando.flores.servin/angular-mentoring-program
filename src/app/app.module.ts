import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { SectionComponent } from './components/section/section.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { CoursesComponent } from './components/courses/courses.component';
import { CourseItemComponent } from './components/courses/course-item/course-item.component';
import { LogoComponent } from './components/header/logo/logo.component';
import { ActionsComponent } from './components/header/actions/actions.component';
import { LoadMoreComponent } from './components/courses/load-more/load-more.component';
import { DurationPipe } from './pipes/duration.pipe';
import { SearchCoursePipe } from './pipes/search-course.pipe';
import { FormsModule } from '@angular/forms';
import { OderByTitlePipe } from './pipes/oder-by-title.pipe';
import { NoDataMessageComponent } from './components/no-data-message/no-data-message.component';
import { NewCoursesDirective } from './directives/new-courses.directive';
import { DeleteCourseComponent } from './components/courses/delete-course/delete-course.component';
import { LoginComponent } from './components/login/login.component';
import { NewCourseComponent } from './components/courses/new-course/new-course.component';
import { EditCourseComponent } from './components/courses/edit-course/edit-course.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AuthInterceptorService } from './interceptors/auth-interceptor.service';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { myReducer } from './app.reducer';

import { ReactiveFormsModule } from '@angular/forms';
import { InputDateComponent } from './components/courses/input-date/input-date.component';
import { InputLengthComponent } from './components/courses/input-length/input-length.component';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Observable } from 'rxjs';

// tslint:disable-next-line:typedef
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/langluages/', '.json');
  // return new TranslateHttpLoader(http);
}

@Injectable()
export class CustomTranslateLoader implements TranslateLoader  {
    contentHeader = new Headers({'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});

    constructor(private http: HttpClient) {}
    getTranslation(lang: string): Observable<any>{
        const apiAddress = '/static/i18n/' + lang + '.json';
        return Observable.create(observer => {
          this.http.get(apiAddress).subscribe((res: Response) => {
                    observer.next(res.json());
                    observer.complete();
                },
            error => {
                //  failed to retrieve from api, switch to local
                this.http.get('/assets/i18n/en.json').subscribe((res: Response) => {
                    observer.next(res.json());
                    observer.complete();
                });
            }
            );
        });
    }
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BreadcrumbsComponent,
    SectionComponent,
    CoursesComponent,
    FooterComponent,
    CourseItemComponent,
    LogoComponent,
    ActionsComponent,
    LoadMoreComponent,

    DurationPipe,
    SearchCoursePipe,
    OderByTitlePipe,
    NoDataMessageComponent,
    NewCoursesDirective,
    DeleteCourseComponent,
    LoginComponent,
    NewCourseComponent,
    EditCourseComponent,
    PageNotFoundComponent,
    InputDateComponent,
    InputLengthComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    StoreModule.forRoot({
      courses: myReducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
      // loader: {
      //   provide: TranslateLoader,
      //   useClass: CustomTranslateLoader,
      //   deps: [HttpClient]
      // }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
