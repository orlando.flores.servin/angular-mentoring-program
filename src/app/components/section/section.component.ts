import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent {
  public searchValue: string;
  @Output() searchCourseEventEmmiter: EventEmitter<string> = new EventEmitter();
  @Output() openNewCourseModalEventEmmiter: EventEmitter<null> = new EventEmitter();

  constructor() {
    this.searchValue = '';
  }

  searchCourse(): void {
    this.searchCourseEventEmmiter.emit(this.searchValue);
  }

  addCourse(): void {
    this.openNewCourseModalEventEmmiter.emit();
    console.log('addCourse()');
  }

}
