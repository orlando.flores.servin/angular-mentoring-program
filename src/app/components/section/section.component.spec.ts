import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { SectionComponent } from './section.component';
import { By } from '@angular/platform-browser';

describe('SectionComponent', () => {
  let component: SectionComponent;
  let fixture: ComponentFixture<SectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ SectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('searchCourse should be called', () => {
    spyOn(component, 'searchCourse');
    const searchButton = fixture.debugElement.query(By.css('.search-button'));
    searchButton.triggerEventHandler('click', null);
    expect(component.searchCourse).toHaveBeenCalled();
  });

  it('addCourse should be called', () => {
    spyOn(component, 'addCourse');
    const editButton = fixture.debugElement.query(By.css('.add-button'));
    editButton.triggerEventHandler('click', null);
    expect(component.addCourse).toHaveBeenCalled();
  });

});
