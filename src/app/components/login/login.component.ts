import { Component, OnInit } from '@angular/core';
import { AuthorizationServiceImpl } from 'src/app/services/authorization.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import * as CourseActions from './../../app.actions';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  coursesRedux: Observable<any>;
  user: User;
  loginForm: FormGroup;
  loginError: string;

  constructor(private router: Router, private authorizationServiceImpl: AuthorizationServiceImpl, private store: Store<AppState>) {
    this.coursesRedux = store.select('courses');
  }

  ngOnInit() {
    this.initLoginForm();
    this.user = new User();
  }
  initLoginForm(): void {
    this.loginForm = new FormGroup({
      login: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  login(): void {
    this.user.login = this.loginForm.value.login;
    this.user.password = this.loginForm.value.password;

    this.authorizationServiceImpl.login(this.user).subscribe(
      (wsResponse: any) => {
        this.user.fakeToken = wsResponse.token;
        if (this.user.fakeToken !== null) {
          this.authorizationServiceImpl.getUserInfo(this.user.fakeToken).subscribe(
            (userInfo: User) => {
              this.authorizationServiceImpl.setUserInfoOnLocalStorage(userInfo);
              this.store.dispatch(new CourseActions.Login(userInfo));
              this.router.navigate(['/courses']);
            }
          );
        }
      },
      (error: any) => {
        this.loginError = error.error;
        setTimeout(() => {
          this.loginError = ''; }, 1500);
        }
    );
  }
}
