import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Course } from 'src/app/models/course';
declare var $: any;
@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.css'],
  changeDetection : ChangeDetectionStrategy.OnPush // https://stackoverflow.com/questions/45815994/what-does-strategy-onpush-mean
})
export class CourseItemComponent {
  @Input() courseItem: Course;
  @Output() openEditCourseModalEventEmmiter: EventEmitter<Course> = new EventEmitter();
  @Output() openModalForDeletingCourseEventEmmiter: EventEmitter<Course> = new EventEmitter();

  constructor() {
  }

  editCourse(): void {
    this.openEditCourseModalEventEmmiter.emit(this.courseItem);
  }

  openModalForDeletingCourse(): void {
    this.openModalForDeletingCourseEventEmmiter.emit(this.courseItem);
  }
}
