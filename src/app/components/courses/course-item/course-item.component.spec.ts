import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CourseItemComponent } from './course-item.component';
import { DurationPipe } from 'src/app/pipes/duration.pipe';
import { By } from '@angular/platform-browser';
import { CourseServiceImpl } from 'src/app/services/course.service';
import { Course } from 'src/app/models/course';
import { NewCoursesDirective } from 'src/app/directives/new-courses.directive';

describe('CourseItemComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        CourseItemComponent,
        DurationPipe,
        NewCoursesDirective
      ],
      providers: [CourseServiceImpl],
    })
    .compileComponents();
  }));

  it('course-item-component should create', () => {
    let courseItemFixture: ComponentFixture<CourseItemComponent>;
    courseItemFixture = TestBed.createComponent(CourseItemComponent);
    const courseItemComponent: CourseItemComponent = courseItemFixture.componentInstance;
    expect(courseItemComponent).toBeTruthy();
  });

  describe('editCourse', () => {
    let courseItemComponent: CourseItemComponent;
    let editCourseEventEmmiterMock;
    let course: Course;

    beforeEach(() => {
      courseItemComponent = new CourseItemComponent();
      editCourseEventEmmiterMock =  {
        emit: jasmine.createSpy('emit')
      };
      course = {
        id: 3,
        title: 'TDD',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD) is a software development process',
        topRated: true
      };
      courseItemComponent.courseItem = course;
      courseItemComponent.openEditCourseModalEventEmmiter = editCourseEventEmmiterMock;
    });

    it('should emit edit course event with course id', () => {
      courseItemComponent.editCourse();
      expect(editCourseEventEmmiterMock.emit).toHaveBeenCalledWith(course);
    });
  });



  describe('deleteCourse', () => {
    let courseItemComponent: CourseItemComponent;
    let deleteCourseEventEmmiterMock;
    let course: Course;

    beforeEach(() => {
      courseItemComponent = new CourseItemComponent();
      deleteCourseEventEmmiterMock =  {
        emit: jasmine.createSpy('emit')
      };
      course = {
        id: 3,
        title: 'TDD',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD) is a software development process',
        topRated: true
      };
      courseItemComponent.courseItem = course;
      courseItemComponent.openModalForDeletingCourseEventEmmiter = deleteCourseEventEmmiterMock;
    });

    it('should emit open modal for deleting course event with course id', () => {
      courseItemComponent.openModalForDeletingCourse();
      expect(deleteCourseEventEmmiterMock.emit).toHaveBeenCalledWith(course);
    });
  });

  describe('topRated', () => {
    let courseItemFixture: ComponentFixture<CourseItemComponent>;
    let courseItemComponent: CourseItemComponent;
    let course: Course;

    beforeEach(() => {
      courseItemFixture = TestBed.createComponent(CourseItemComponent);
      course = {
        id: 3,
        title: 'TDD',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD) is a software development process',
        topRated: true
      };

      courseItemComponent = courseItemFixture.componentInstance;
      courseItemComponent.courseItem = course;
    });

    it('should be shown a star', () => {
      courseItemFixture.detectChanges();
      const starIcon = courseItemFixture.debugElement.query(By.css('.star'));
      expect(starIcon).toBeTruthy();
    });

    it('should not be shown a star', () => {
      courseItemComponent.courseItem.topRated = false;
      courseItemFixture.detectChanges();
      const starIcon = courseItemFixture.debugElement.query(By.css('.star'));
      expect(starIcon).toBeFalsy();
    });
  });


  describe('Title', () => {
    let courseItemFixture: ComponentFixture<CourseItemComponent>;
    let courseItemComponent: CourseItemComponent;
    let course: Course;

    beforeEach(() => {
      courseItemFixture = TestBed.createComponent(CourseItemComponent);
      course = {
        id: 3,
        title: 'tdd',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD) is a software development process',
        topRated: true
      };
      courseItemComponent = courseItemFixture.componentInstance;
      courseItemComponent.courseItem = course;
    });

    it('should convert title course to uppercase', () => {
      courseItemFixture.detectChanges();
      const hostElement = courseItemFixture.nativeElement;
      const title: HTMLElement = hostElement.querySelector('h5');
      expect(title.textContent).toBe('TDD' + 'star');
    });
  });
});


