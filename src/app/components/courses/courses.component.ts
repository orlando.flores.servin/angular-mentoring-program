import { Component, OnInit, ViewChild, EventEmitter, Output, AfterViewChecked } from '@angular/core';
import { Course } from 'src/app/models/course';
import { SearchCoursePipe } from 'src/app/pipes/search-course.pipe';
import { CourseServiceImpl } from 'src/app/services/course.service';
import { DeleteCourseComponent } from './delete-course/delete-course.component';
import { EditCourseComponent } from './edit-course/edit-course.component';
import { HeaderComponent } from '../header/header.component';
import { BreadcrumbsComponent } from '../breadcrumbs/breadcrumbs.component';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import * as CourseActions from './../../app.actions';


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
  providers: [ SearchCoursePipe ]
})
export class CoursesComponent implements OnInit {
  @ViewChild(DeleteCourseComponent, {static: false}) deleteCourseComponent: DeleteCourseComponent;
  @ViewChild(EditCourseComponent, {static: false}) editCourseComponent: EditCourseComponent;
  @ViewChild(HeaderComponent, {static: false}) headerComponent: HeaderComponent;
  @ViewChild(BreadcrumbsComponent, {static: false}) breadcrumbsComponent: BreadcrumbsComponent;

  coursesRedux: Observable<any>;
  courses: Course[] = [];

  searchValue: string;
  course: Course;

  constructor(private courseService: CourseServiceImpl,
              private searchCoursePipe: SearchCoursePipe,
              private store: Store<AppState>) {
    this.coursesRedux = store.select('courses');
  }

  ngOnInit(): void {
    this.getCourses();
    this.searchValue = '';
    /*this.courseService.onCreateCourse.subscribe((newItem) => {
      this.courses.push(newItem);
      this.getCourses();
    });*/
    this.getCourses();
  }

  getCourses(): void {
   this.courseService.getCourses().subscribe((wsResponse: Course[]) => {
     this.courses = wsResponse;
    //  Nos permite emitir un mensaje o accion, este recibe un objecto con los atributos type para describir la accion que se ejecuta
    // y el payload para enviar la informacion
     this.store.dispatch(new CourseActions.SetCourse(wsResponse));
   });
  }

  searchCourse(searchValue: string): void {
    this.searchValue = searchValue;
    this.courseService.searchCourseByTextFragment(searchValue).subscribe((wsResponse: Course[]) => {
      this.courses = wsResponse;
    });
  }

  deleteCourse(id: number) {
   this.courseService.removeItem(id).subscribe((wsResponse) => {
    this.courseService.count = this.courseService.count - 1;
    this.getCourses();
    this.deleteCourseComponent.closeModal();
   });
  }

  loadMore(message: string ): void {
    this.courseService.count = this.courseService.count + 1;
    this.getCourses();
  }

  openModalForDeletingCourse(course: Course) {
    this.deleteCourseComponent.openModal(course);
  }

  updateBreadcrumbsTitle(title: string): void {
    console.log(title);
    this.breadcrumbsComponent.title = title;
  }

}
