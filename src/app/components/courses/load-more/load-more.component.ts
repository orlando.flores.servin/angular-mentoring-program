import { Component, EventEmitter, Output } from '@angular/core';
import { CourseServiceImpl } from 'src/app/services/course.service';

@Component({
  selector: 'app-load-more',
  templateUrl: './load-more.component.html',
  styleUrls: ['./load-more.component.css']
})
export class LoadMoreComponent {
  @Output() loadMoreEventEmmiter: EventEmitter<string> = new EventEmitter();

  constructor() { }
  loadMore(): void {
    this.loadMoreEventEmmiter.emit(`Loading more`);
  }

}
