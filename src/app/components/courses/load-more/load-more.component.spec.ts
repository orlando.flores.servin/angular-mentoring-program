import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { LoadMoreComponent } from './load-more.component';

describe('LoadMoreComponent', () => {
  let loadMoreComponent: LoadMoreComponent;
  let loadMoreFixture: ComponentFixture<LoadMoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadMoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    loadMoreFixture = TestBed.createComponent(LoadMoreComponent);
    loadMoreComponent = loadMoreFixture.componentInstance;
    loadMoreFixture.detectChanges();
  });

  it('should create', () => {
    expect(loadMoreComponent).toBeTruthy();
  });

  it('loadMore should be called', () => {
    spyOn(loadMoreComponent, 'loadMore');
    const loadMoreButton = loadMoreFixture.debugElement.query(By.css('.load-more'));
    loadMoreButton.triggerEventHandler('click', null);
    expect(loadMoreComponent.loadMore).toHaveBeenCalled();
  });

});
