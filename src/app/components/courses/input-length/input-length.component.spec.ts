import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputLengthComponent } from './input-length.component';

describe('InputLengthComponent', () => {
  let component: InputLengthComponent;
  let fixture: ComponentFixture<InputLengthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputLengthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputLengthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
