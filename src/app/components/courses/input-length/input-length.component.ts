import { Component, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, FormControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-input-length',
  templateUrl: './input-length.component.html',
  providers : [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputLengthComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: InputLengthComponent,
      multi: true
    }
  ]
})
export class InputLengthComponent implements ControlValueAccessor, Validator {
  length = '';
  onChange = (length: string) => { };
  onTouch = () => { };

  constructor() { }

  onInput(length: string) {
    this.length = length;
    this.onTouch();
    this.onChange(this.length);
  }

  writeValue(length: any): void {
    if (length) {
      this.length = length;
    } else {
      this.length = '';
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  validate(control: FormControl): ValidationErrors {
    console.log(`_${control.value}_${control.value.length}`);
    if (!(control.value.length > 0)) {
      return { required: '* Required' };
    }
    if (!control.value.match('^[0-9]*$')) {
      return { date: '* Only numbers allow' };
    }
    return null;
  }
}
