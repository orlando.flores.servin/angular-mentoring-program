import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesComponent } from './courses.component';
import { CourseItemComponent } from './course-item/course-item.component';
import { LoadMoreComponent } from './load-more/load-more.component';
import { SectionComponent } from '../section/section.component';
import { SearchCoursePipe } from 'src/app/pipes/search-course.pipe';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DurationPipe } from 'src/app/pipes/duration.pipe';
import { FormsModule } from '@angular/forms';
import { NoDataMessageComponent } from '../no-data-message/no-data-message.component';
import { OderByTitlePipe } from 'src/app/pipes/oder-by-title.pipe';
import { Course } from 'src/app/models/course';
import { NewCoursesDirective } from 'src/app/directives/new-courses.directive';
import { CourseServiceImpl } from 'src/app/services/course.service';
import { DeleteCourseComponent } from './delete-course/delete-course.component';

describe('CoursesComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [
        CoursesComponent,
        CourseItemComponent,
        LoadMoreComponent,
        SectionComponent,
        NoDataMessageComponent,
        DeleteCourseComponent,

        SearchCoursePipe,
        DurationPipe,
        OderByTitlePipe,
        NewCoursesDirective
      ],
      providers: [CourseServiceImpl, SearchCoursePipe],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  it('should be created CoursesComponent', () => {
    let coursesComponentFixture: ComponentFixture<CoursesComponent>;
    let coursesComponent: CoursesComponent;
    coursesComponentFixture = TestBed.createComponent(CoursesComponent);
    coursesComponent = coursesComponentFixture.componentInstance;
    expect(coursesComponent).toBeTruthy();
  });

  describe('searchCourse', () => {
    let coursesComponentFixture: ComponentFixture<CoursesComponent>;
    let coursesComponent: CoursesComponent;

    beforeEach(() => {
      coursesComponentFixture = TestBed.createComponent(CoursesComponent);
      coursesComponent = coursesComponentFixture.componentInstance;
    });

    it('should set searchValue', () => {
      coursesComponent.searchCourse('TDD');
      expect(coursesComponent.searchValue).toEqual('TDD');
    });
  });

  /*

  describe('editCourse', () => {
    let coursesComponentFixture: ComponentFixture<CoursesComponent>;
    let coursesComponent: CoursesComponent;
    const id = 3;
    const tddCourse = {
      id: 3,
      title: 'TDD',
      creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
      duration: 250,
      description: 'Test-driven development (TDD)',
      topRated: true
    };

    beforeEach(() => {
      coursesComponentFixture = TestBed.createComponent(CoursesComponent);
      coursesComponent = coursesComponentFixture.componentInstance;
    });

    it('should log course id when editCourse() is invoked ', () => {
      spyOn(console, 'log');
      coursesComponent.editCourse(tddCourse);
      expect(console.log).toHaveBeenCalledWith(`edit course with id = ${id}`);
    });
  });
  */


  describe('deleteCourse', () => {
    let coursesComponentFixture: ComponentFixture<CoursesComponent>;
    let deleteCourseComponentFixture: ComponentFixture<DeleteCourseComponent>;
    let coursesComponent: CoursesComponent;
    let courseService: CourseServiceImpl;
    const id = 1;
    let angularCourse: Course;
    let tddCourse: Course;

    beforeEach(() => {
      courseService = TestBed.get(CourseServiceImpl);
      tddCourse = {
        id: 3,
        title: 'TDD',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD)',
        topRated: true
      };
      angularCourse = {
        id: 1,
        title: 'Angular 01',
        creationDate: new Date(2019, 5, 20, 0, 0, 0, 0),
        duration: 90,
        description: 'Angular is a platform for building mobile and desktop web applications.',
        topRated: false
      };
      courseService.courses = [tddCourse, angularCourse];
      coursesComponentFixture = TestBed.createComponent(CoursesComponent);
      coursesComponent = coursesComponentFixture.componentInstance;

      deleteCourseComponentFixture = TestBed.createComponent(DeleteCourseComponent);
      coursesComponent.deleteCourseComponent = deleteCourseComponentFixture.componentInstance;

      coursesComponent.courses = courseService.getCourses();
      coursesComponent.filtredCourses = coursesComponent.courses;
    });

    it('should delete a course by id', () => {
      coursesComponent.deleteCourse(tddCourse.id);
      expect(coursesComponent.courses.length).toEqual(1);
      expect(coursesComponent.courses[0].id).toEqual(angularCourse.id);
      expect(coursesComponent.filtredCourses[0].id).toEqual(angularCourse.id);
    });
  });


  describe('loadMore', () => {
    let coursesComponentFixture: ComponentFixture<CoursesComponent>;
    let coursesComponent: CoursesComponent;
    const id = 1;

    beforeEach(() => {
      coursesComponentFixture = TestBed.createComponent(CoursesComponent);
      coursesComponent = coursesComponentFixture.componentInstance;
    });

    it('should log loading more...', () => {
      const message = 'loading more...';
      spyOn(console, 'log');
      coursesComponent.loadMore(message);
      expect(console.log).toHaveBeenCalledWith(message);
    });
  });

  describe('openModalForDeletingCourse', () => {
    let coursesComponentFixture: ComponentFixture<CoursesComponent>;
    let coursesComponent: CoursesComponent;
    let tddCourse: Course;

    beforeEach(() => {
      coursesComponentFixture = TestBed.createComponent(CoursesComponent);
      coursesComponent = coursesComponentFixture.componentInstance;
      tddCourse = {
        id: 3,
        title: 'TDD',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD)',
        topRated: true
      };
    });

    it('should open the modal for Deleting courses', () => {
      spyOn(coursesComponent, 'openModalForDeletingCourse');
      coursesComponent.openModalForDeletingCourse(tddCourse);
      expect(coursesComponent.openModalForDeletingCourse).toHaveBeenCalledWith(tddCourse);
    });
  });


  describe('ngOnInit', () => {
    let coursesComponentFixture: ComponentFixture<CoursesComponent>;
    let coursesComponent: CoursesComponent;

    beforeEach(() => {
      coursesComponentFixture = TestBed.createComponent(CoursesComponent);
      coursesComponent = coursesComponentFixture.componentInstance;
    });

    it('should init courses and search value', () => {
      coursesComponent.ngOnInit();
      expect(coursesComponent.courses.length).toEqual(3);
      expect(coursesComponent.filtredCourses.length).toEqual(3);
      expect(coursesComponent.searchValue).toEqual('');
    });
  });
});
