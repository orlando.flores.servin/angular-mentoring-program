import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Course } from 'src/app/models/course';
import { CourseServiceImpl } from 'src/app/services/course.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html'
})
export class EditCourseComponent implements OnInit {

  editCourseForm: FormGroup;
  course: Course;
  creationDate: string;
  @Output() editCourseEventEmmiter: EventEmitter<Course> = new EventEmitter();
  @Output() updateBreadcrumbsTitleEventEmmiter: EventEmitter<string> = new EventEmitter();

  constructor(private courseService: CourseServiceImpl, private activatedRoute: ActivatedRoute, private router: Router) {
    this.initCourse();
    this.initForm();
   }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const id = Number(params.get('id'));
      if (id > 0) {
        this.courseService.getItemById(id).subscribe((course: Course) => {
          course.authors = {id: 0, name: 'Mentor' };
          course.date = `05-20-2020`,
          this.course = course;
          this.editCourseForm.setValue(course);
          this.openModal();
        });
      } else {}
    });
  }

  initForm() {
    this.editCourseForm = new FormGroup({
      id: new FormControl(0),
      name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      date: new FormControl(''),
      length: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      description: new FormControl('', [Validators.required, Validators.maxLength(500)]),
      authors : new FormGroup({
        id: new FormControl(0),
        name: new FormControl('', [Validators.required, Validators.maxLength(500)])
      }),
      isTopRated: new FormControl(false),
    });
  }

  initCourse(): void {
    this.course = {
      id: null,
      name: null,
      date: null,
      length: null,
      description: '',
      authors: {
        id: 0,
        name: null
      },
      topRated: false
    };
  }


  public openModal(): void {
    $('#editCourse').modal({ backdrop: 'static', keyboard: false, show: true});
  }

  closeModal(): void {
    $('#editCourse').modal('hide');
  }

  editCourse(): void {
    this.course = this.editCourseForm.value;
    this.courseService.updateItem(this.course).subscribe((course: Course) => {
      this.closeModal();
      this.router.navigate(['/courses']);
    });
  }

  convertStringToDate(): Date {
    const dateArray = this.creationDate.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/);
    return (dateArray) ? new Date(Number(dateArray[3]), Number(dateArray[1]) - 1, Number(dateArray[2])) : null;
  }

  updateBreadcrumbsTitle() {
    this.updateBreadcrumbsTitleEventEmmiter.emit(this.course.name);
  }

  convertDate(date: string) {
    const dateArray = date.split('T')[0].split('-');
    console.log(dateArray)
    return `${dateArray[1]}-${dateArray[2]}-${dateArray[0]}`;
  }
}
