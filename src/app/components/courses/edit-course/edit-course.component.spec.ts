import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCourseComponent } from './edit-course.component';
import { Course } from 'src/app/models/course';
import { DurationPipe } from 'src/app/pipes/duration.pipe';
import { FormsModule } from '@angular/forms';

describe('EditCourseComponent', () => {


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [
        EditCourseComponent,
        DurationPipe
      ]
    })
    .compileComponents();
  }));


  describe('Edit course  modal', () => {

    let editCourseComponentFixture: ComponentFixture<EditCourseComponent>;
    let editCourseComponent: EditCourseComponent;
    let course: Course;
    let editCourseEventEmmiterMock;

    beforeEach(() => {
      editCourseComponentFixture = TestBed.createComponent(EditCourseComponent);
      editCourseEventEmmiterMock =  {
        emit: jasmine.createSpy('emit')
      };

      course = {
        id: 3,
        title: 'TDD',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD)',
        topRated: true
      };

      editCourseComponent = editCourseComponentFixture.componentInstance;
      editCourseComponent.course = course;
      editCourseComponent.editCourseEventEmmiter = editCourseEventEmmiterMock;
    });

    it('should create', () => {
      editCourseComponentFixture.detectChanges();
      expect(editCourseComponent).toBeTruthy();
    });

    /*it('should open edit modal', () => {
      editCourseComponent.openModal(course);
      editCourseComponentFixture.detectChanges();
      const title = editCourseComponentFixture.nativeElement.querySelector('.title').innerHTML;
      expect(`Edit course: ${course.title}`).toEqual(title);
    });*/

  });
});
