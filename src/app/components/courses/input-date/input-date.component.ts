import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, FormControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-input-date',
  templateUrl: './input-date.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputDateComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: InputDateComponent,
      multi: true
    }
  ],
})

export class InputDateComponent implements ControlValueAccessor, Validator {
  value = '';
  isDisabled: boolean;
  onChange = (_: any) => { }; // Esto hará que Angular sepa el valor cada vez que escribimos
  onTouch = () => { };

  constructor() { }

  onInput(date: string) {
    console.log(`onInput ${date}`);
    this.value = date;
    this.onTouch();
    this.onChange(this.value);
  }

  // @Input() disabled = false;

  // Trae el valor puesto en un ngModel, ReactiveForm, etc. Ese valor podemos pasárselo a nuestro input nativo.
  writeValue(date: any): void {
    console.log(`writeValue ${date}`);
    if (date) {
      this.value = date;
    } else {
      this.value = '';
    }
  }

  // Registramos una funcion que luego usamos para informarle a Angular cuál es el valor de nuestro Form Control.
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  // Registramos una función que luego ejecutaremos cuando necesitemos que nuestro Form Control tenga el estado touched
  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }
  // Trae el estado disabled desde nuestro Reactive Form. Podemos usar este estado para pasarselo a nuesto input nativo.
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  validate(control: FormControl): ValidationErrors {
    console.log(`_${control.value}_${control.value.length}`);
    if (!(control.value.length > 0)) {
      return { required: '* Required' };
    }
    if (!control.value.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/)) {
      return { date: '* Date format should be mm-dd-yyyy' };
    }
    return null;
  }
}
