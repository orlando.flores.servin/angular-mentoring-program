import { Component, Output, EventEmitter } from '@angular/core';
import { Course } from 'src/app/models/course';

declare var $: any;

@Component({
  selector: 'app-delete-course',
  templateUrl: './delete-course.component.html'
})
export class DeleteCourseComponent {
  @Output() deleteCourseEventEmmiter: EventEmitter<number> = new EventEmitter();
  courseItem: Course;

  constructor() { }

  deleteCourse(): void {
    this.deleteCourseEventEmmiter.emit(this.courseItem.id);
  }

  openModal(course: Course): void {
    this.courseItem = course;
    $('#deleteCourse').modal('show');
  }

  closeModal(): void {
    $('#deleteCourse').modal('hide');
  }

}
