import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DeleteCourseComponent } from './delete-course.component';
import { Course } from 'src/app/models/course';

describe('DeleteCourseComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteCourseComponent ]
    })
    .compileComponents();
  }));


  describe('Delete modal', () => {
    let deleteCourseComponentFixture: ComponentFixture<DeleteCourseComponent>;
    let deleteCourseComponent: DeleteCourseComponent;
    let course: Course;
    let deleteCourseEventEmmiterMock;


    beforeEach(() => {
      deleteCourseComponentFixture = TestBed.createComponent(DeleteCourseComponent);
      deleteCourseEventEmmiterMock =  {
        emit: jasmine.createSpy('emit')
      };

      course = {
        id: 3,
        title: 'TDD',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD)',
        topRated: true
      };

      deleteCourseComponent = deleteCourseComponentFixture.componentInstance;
      deleteCourseComponent.courseItem = course;
      deleteCourseComponent.deleteCourseEventEmmiter = deleteCourseEventEmmiterMock;
    });

    it('should create', () => {
      deleteCourseComponentFixture.detectChanges();
      expect(deleteCourseComponent).toBeTruthy();
    });

    it('should open delete modal', () => {
      deleteCourseComponent.openModal(course);
      deleteCourseComponentFixture.detectChanges();
      const title = deleteCourseComponentFixture.nativeElement.querySelector('.title').innerHTML;
      expect(`Are you sure you want to delete ${course.title}?`).toEqual(title);
    });

    it('should close delete modal', () => {
      spyOn(deleteCourseComponent, 'closeModal');
      deleteCourseComponent.closeModal();
      expect(deleteCourseComponent.closeModal).toHaveBeenCalled();
    });

    it('should emit delete course event with course id', () => {
      deleteCourseComponent.deleteCourse();
      expect(deleteCourseEventEmmiterMock.emit).toHaveBeenCalledWith(course.id);
    });
  });
});
