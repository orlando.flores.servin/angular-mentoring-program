import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCourseComponent } from './new-course.component';
import { Course } from 'src/app/models/course';
import { DurationPipe } from 'src/app/pipes/duration.pipe';
import { FormsModule } from '@angular/forms';

describe('NewCourseComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [
        NewCourseComponent,
        DurationPipe
      ]
    })
    .compileComponents();
  }));

  describe('New course modal', () => {
    let newCourseComponentFixture: ComponentFixture<NewCourseComponent>;
    let newCourseComponent: NewCourseComponent;
    let course: Course;
    let addCourseEventEmmiterMock;

    beforeEach(() => {
      newCourseComponentFixture = TestBed.createComponent(NewCourseComponent);
      addCourseEventEmmiterMock =  {
        emit: jasmine.createSpy('emit')
      };

      course = {
        id: 3,
        title: 'TDD',
        creationDate: new Date(2019, 10, 10, 0, 0, 0, 0),
        duration: 250,
        description: 'Test-driven development (TDD)',
        topRated: true
      };

      newCourseComponent = newCourseComponentFixture.componentInstance;
      newCourseComponent.course = course;
      newCourseComponent.addCourseEventEmmiter = addCourseEventEmmiterMock;
    });

    it('should create', () => {
      newCourseComponentFixture.detectChanges();
      expect(newCourseComponent).toBeTruthy();
    });

  });
});
