import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Course } from 'src/app/models/course';
import { Router } from '@angular/router';
import { CourseServiceImpl } from 'src/app/services/course.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import * as CourseActions from './../../../app.actions';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-new-course',
  templateUrl: './new-course.component.html'
})
export class NewCourseComponent implements OnInit {


  course: Course;
  creationDate = '';
  coursesForm: FormGroup;

  @Output() addCourseEventEmmiter: EventEmitter<Course> = new EventEmitter();
  coursesRedux: Observable<any>;

  constructor(private router: Router, private courseService: CourseServiceImpl, private store: Store<AppState>) {
    this.coursesRedux = store.select('courses');
  }

  ngOnInit() {
    this.initForm();
    this.openModal();
  }

  initForm() {
    this.coursesForm = new FormGroup({
      id: new FormControl(0),
      name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      date: new FormControl(''),
      length: new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
      description: new FormControl('', [Validators.required, Validators.maxLength(500)]),
      authors : new FormGroup({
        id: new FormControl(0),
        name: new FormControl('', [Validators.required, Validators.maxLength(500)])
      }),
      isTopRated: new FormControl(false),
    });
  }

  openModal(): void {
    $('#newCourse').modal({ backdrop: 'static', keyboard: false, show: true});
  }

  closeModal(): void {
    $('#newCourse').modal('hide');
  }

  addCourse(): void {
    this.course = this.coursesForm.value;
    console.log(this.course);
    this.course.date = this.convertStringToDate();
    this.courseService.createCourse(this.course).subscribe((wsResponse: Course) => {
      this.courseService.count = (this.courseService.count + 1);
      // this.courseService.onCreateCourse.emit(wsResponse);
      this.closeModal();
      this.store.dispatch(new CourseActions.AddCourse(wsResponse));
      this.router.navigate(['/courses']);
    });
  }

  convertStringToDate(): string {
    const dateArray = this.creationDate.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/);
    const date = (dateArray) ? new Date(Number(dateArray[3]), Number(dateArray[1]) - 1, Number(dateArray[2])) : new Date();
    // tslint:disable-next-line: max-line-length
    return `${date.getFullYear()}-${date.getMonth() + 1 }-${date.getDate()}T0:0:0+00:00`;
  }

}
