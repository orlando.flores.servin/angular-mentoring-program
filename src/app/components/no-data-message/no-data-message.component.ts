import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-data-message',
  templateUrl: './no-data-message.component.html'
})
export class NoDataMessageComponent implements OnInit {

  MESSAGE = 'No data, feel free to add a new course';
  constructor() { }

  ngOnInit() {
  }

}
