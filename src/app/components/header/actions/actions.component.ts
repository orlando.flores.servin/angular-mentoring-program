import { Component} from '@angular/core';
import { AuthorizationServiceImpl } from 'src/app/services/authorization.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.css']
})
export class ActionsComponent {

  userInfo: User;

  showLinks: boolean;
  constructor(private authorizationService: AuthorizationServiceImpl, private router: Router) {
    this.userInfo = this.authorizationService.getUserInfoFromLocalStorage();
    this.showLinks = authorizationService.isAuthenticated();
  }

  logout() {
    if (this.authorizationService.logout()) {
      this.router.navigate(['/login']);
    }
  }
}
