import { Component, OnInit } from '@angular/core';
import { AuthorizationServiceImpl } from 'src/app/services/authorization.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit  {
  showActions: boolean;

  constructor(private authorizationService: AuthorizationServiceImpl) { }

  ngOnInit(): void {
    this.showActions = this.authorizationService.isAuthenticated();
  }

}
