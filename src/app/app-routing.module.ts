import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CoursesComponent } from './components/courses/courses.component';
import { LoginComponent } from './components/login/login.component';
import { NewCourseComponent } from './components/courses/new-course/new-course.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AuthGuardService } from './services/auth-guard.service';
import { EditCourseComponent } from './components/courses/edit-course/edit-course.component';

const routes: Routes = [
  { path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    component: LoginComponent, },
  {
    path: 'courses',
    component: CoursesComponent,
    children: [
      {
        path: 'new',
        component: NewCourseComponent,
        canActivate: [AuthGuardService]
      },
    ],
    canActivate: [AuthGuardService]
  },
  {
    path: 'courses/:id',
    component: EditCourseComponent,
    canActivate: [AuthGuardService]
  },
  { path: '404', component: PageNotFoundComponent},
  { path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
