import { Action } from '@ngrx/store';
import { Course } from './models/course';
import * as CourseActions from './app.actions';
import { User } from './models/user';

// Definimos un modelo para guardar los datos
export interface AppState {
    courses: Course[];
    user: User;
}

// inicializamos el modelo
export const initialState = {
    courses : [],
    user: null
};

// Declaramos el reducer que se manda a llamar en el app.module, el cual recibe 2 parametros
// state (que seria el model AppState)  y action
export function myReducer(state: AppState = initialState, action: CourseActions.Actions) {
    console.log(action.type);
    console.log(action.payload);

    switch (action.type) {
        case CourseActions.LOGIN:
            return {
                ...state, user: action.payload
            };
        case CourseActions.GET_USER:
            return  {
                ...state
            };
        case CourseActions.SET_COURSES:
            return {
                ...state, courses: action.payload, user: state.user
                // ...state, courses: action.payload, user: state.user
            };
        case CourseActions.ADD_COURSES:
            state.courses.push(action.payload);
            return {
            ...state, courses: state.courses
            };
            /*
            return {
            ...state, courses: action.payload
            }; */
        default:
            return state;
    }

}


